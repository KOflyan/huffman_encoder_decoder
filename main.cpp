#include <bitset>
#include "main.h"
#include "encoder/encoder.h"
#include "decoder/decoder.h"
#include "io/io_utils.h"
#include "common/common.h"
#include <cassert>

using namespace std;
using pNode = shared_ptr<Node>;
namespace po = boost::program_options;


Main::Main() {
    this->performChecks();
}

po::variables_map Main::initializeVariableMap(int argc, const char **argv) {
    string input;
    vector<string> output;

    po::options_description desc("Allowed options");

    desc.add_options()
            ("help", "Show available options")
            ("input,i", po::value<string>(&input), "Input file")
            ("output,o", po::value< vector<string>>(&output), "Output file / output + input file")
            ("decode", "Flag for decoding (encode mode by default)")
            ;

    po::positional_options_description p;
    p.add("input", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << endl;
        exit(0);
    }

    return vm;
}


void Main::applyAlgorithm(string contents, bool encode) {
    string result;

    if (encode) {
        HuffmanEncoder encoder(contents);
        result = encoder.encodeAndReturn();
    } else {
        HuffmanDecoder decoder(contents);
        result = decoder.decodeAndReturn();
    }

    return this->processor.writeFile(this->io.os, result);
}


void Main::start(po::variables_map &vm) {

    this->io = this->processor.getIOStreams(vm);

    string contents = this->processor.readFile(*this->io.is);
    return this->applyAlgorithm(contents, vm.count("decode") == 0);
}

void Main::performChecks() {
    if (Constants::CONTENT_SEPARATOR.empty()) {
        throw logic_error("Content separator must not be empty.");
    }

    if (Constants::HEADER_SEPARATOR.empty()) {
        throw logic_error("Header separator must not be empty.");
    }
}

int main(int argc, const char** argv) {
    Main m;
    po::variables_map vm = m.initializeVariableMap(argc, argv);
    m.start(vm);
    return 0;
}