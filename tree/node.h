
#ifndef FINAL_NODE_H
#define FINAL_NODE_H

#include <iostream>
#include <memory>


using namespace std;
class Node;
using pNode = shared_ptr<Node>;

class Node {
    public:
        pNode left, right;
        char val;
        int freq;
        explicit Node(char val, int freq);
};



#endif //FINAL_NODE_H
