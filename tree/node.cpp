
#include "node.h"

Node::Node(char val, int freq) {
    this->val = val;
    this->freq = freq;
}


ostream &operator<<(ostream &os, const pNode &node) {
    os << node->val << "|" << node->freq << endl;
    return os;
}