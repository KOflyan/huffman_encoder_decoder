
#ifndef FINAL_ENCODER_H
#define FINAL_ENCODER_H

#include "../codebook/codebook.h"
#include "../tree/node.h"
#include <map>


using namespace std;

/**
 * Class for encoding text using canonical Huffman algorithm.
 */
class HuffmanEncoder {
    private:
        string header;
        string fileContents;
        map<char, int> freqMap;
        map<string, vector<char>> bins;
        HuffmanCodeBook codeBook;
        /**
         * Compose frequency map for every character in the input file.
         * Frequency map = count occurrences for every char in file, save as map: {'A': 26, 'B': 12 ...}
         */
        void composeFrequencyMap();
        /**
         * Using frequency map, create Huffman tree.
         */
        void createTree();
        /**
         * Recursively group contents of Huffman tree into bins.
         *
         * @param node Huffman tree node
         * @param result map of bins we want to fill
         * @param count tree depth count
         * @return map of bins
         */
        map<string, vector<char>> groupIntoBins(pNode node, map<string, vector<char>> &result, int count);
        /**
         * Compose Huffman code book and Huffman header using bins.
         */
        void constructCodeBookAndHeader();
        /**
         * Encode header and contents using code book and hexadecimal system.
         *
         * @return binary string of leftovers
         */
        string encodeHeaderAndContents(string& binary, string& content);
    public:
        HuffmanEncoder() = default;
        explicit HuffmanEncoder(string &fileContents);
        /**
         * Encode file contents and return as string.
         *
         * @return file contents as string
         */
        string encodeAndReturn();

};


#endif //FINAL_ENCODER_H
