
#include "encoder.h"
#include "../common/common.h"
#include "../common/utils.h"
#include <queue>
#include <bitset>
#include <sstream>


HuffmanEncoder::HuffmanEncoder(string &fileContents) {
    this->fileContents = fileContents;
    this->composeFrequencyMap();
    this->createTree();
}


void HuffmanEncoder::composeFrequencyMap() {

    map<char, int> freqMap;

    for (char c : this->fileContents) {
        if (freqMap.find(c) == freqMap.end()) {
            freqMap[c] = 1;
            continue;
        }

        ++freqMap[c];
    }
    this->freqMap = freqMap;
}


void HuffmanEncoder::createTree() {

    auto comparator = []( pNode n1, pNode n2 ) {
        if (n1->freq == n2->freq) {
            if (n1->val == '@' || n2->val == '@') {
                return n2->val == '@';
            }
            locale loc;
            return tolower(n1->val, loc) > tolower(n2->val, loc);
        }
        return n1->freq > n2->freq;
    };

    priority_queue<pNode, vector<pNode>, decltype(comparator)> queue(comparator);


    for (auto& p : this->freqMap) {
        queue.push(make_shared<Node>(p.first, p.second));
    }

    while (queue.size() > 1) {
        auto e1 = queue.top(); queue.pop();
        auto e2 = queue.top(); queue.pop();

        pNode newNode = make_shared<Node>('@', e1->freq + e2->freq);

        newNode->left = e1;
        newNode->right = e2;

        queue.push(newNode);
    }

    this->bins = {};
    this->groupIntoBins(queue.top(), bins, 0);

//    for (auto& b : bins) {
//        cout << b.first << b.second << endl;
//    }
//    cout << endl << endl;
    this->constructCodeBookAndHeader();
}


string HuffmanEncoder::encodeAndReturn() {
    string binary, content;
    this->encodeHeaderAndContents(binary, content);

    while (!binary.empty()) {
        if (binary.size() < 8) {
            content += Utils::toHex(binary) + " ";
            break;
        }
        string sub = binary.substr(0, 8);
        content += Utils::toHex(sub) + " ";
        binary = binary.substr(8, binary.size());
    }

    Utils::rtrim(content);
    return content;
}


string HuffmanEncoder::encodeHeaderAndContents(string& binary, string& content) {

    bool head = true;
    size_t len = this->fileContents.size();

    for (size_t i = 0; i < len; ++i) {

        char c = this->fileContents[i];

        if (head && i < len - Constants::CONTENT_SEPARATOR.size()) {
            string sub = this->fileContents.substr(i, Constants::CONTENT_SEPARATOR.size());

            if (sub == Constants::CONTENT_SEPARATOR) {
                Utils::rtrim(content);
                content += sub;
                i += Constants::CONTENT_SEPARATOR.size() - 1;
                head = false;
                continue;
            }
        }

        if (head) {
            content += Utils::toHex((int) c) + " ";
        } else {
            binary += this->codeBook.getCharCode(c);;

            if (binary.size() >= 8) {
                string sub = binary.substr(0, 8);
                content += Utils::toHex(sub) + " ";
                binary = binary.substr(8, binary.size());
            }
        }
    }

    return binary;
}

map<string, vector<char>> HuffmanEncoder::groupIntoBins(pNode node, map<string, vector<char>> &result, int count) {
    if (node == nullptr) return result;
    char val = node->val;

    string key = to_string(count);
    if (result.find(key) == result.end()) {
        vector<char> v;
        if (val != '@') { v.push_back(val); }
        result[key] = v;
    } else if (val != '@') {
        vector<char> &v = result[key];
        v.push_back(node->val);
        sort(v.begin(), v.end());
    }

    groupIntoBins(node->right, result, count + 1);
    groupIntoBins(node->left, result, count + 1);
    return result;
}


void HuffmanEncoder::constructCodeBookAndHeader() {

    string header1;
    string header2;

    size_t j = 0;
    size_t code = 0;

    for (auto& val : this->bins) {
        if (j == 0) {
            ++j;
            continue;
        }

        vector<char> &symbols = val.second;

        string result = Utils::toBinaryString(code);
        code = (code + symbols.size()) << 1;

        HuffmanCodeBookRow row(result, j - 1, symbols.size(), symbols);
        this->codeBook.append(row);


        header1 += to_string(symbols.size());
        if (j != this->bins.size() - 1) {
            header1 += ", ";
        }

        for (size_t i = 0; i < symbols.size(); i++) {
            header2 += symbols.at(i);
            if (j < this->bins.size() - 1 || i < symbols.size() - 1) {
                header2 += ", ";
            }
        }
        ++j;
    }

//    for (auto& row : this->codeBook.rows) {
//        cout << row.index << " " << row.code << " " << row.numberOfCodes << " " << row.symbols << endl;
//    }

    this->header = header1 + Constants::HEADER_SEPARATOR + header2;

    this->fileContents = this->header + Constants::CONTENT_SEPARATOR + this->fileContents;
}

