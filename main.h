#ifndef FINAL_MAIN_H
#define FINAL_MAIN_H


#include "io/io_utils.h"
#include "tree/node.h"
#include <queue>

using namespace std;

/**
 * Main class, linker for all classes above.
 */
class Main {
    private:
        IOHolder io;
        IOProcessor processor;
    public:
        explicit Main();
        /**
         * Compose boost variable map.
         *
         * @param argc args count from cmd
         * @param argv args values from cmd
         * @return variable map and option descriptions
         */
        po::variables_map initializeVariableMap(int argc, const char** argv);
        /**
         * Apply Huffman encoding/decoding algorithm and write result into file.
         *
         * @param content input file content
         * @param encode is encode mode
         */
        void applyAlgorithm(string content, bool encode=true);
        /**
         * Start the program.
         *
         * @param vm boost variable map
         */
        void start(po::variables_map &vm);
        /** Check if constants are valid. */
        void performChecks();
};


#endif //FINAL_MAIN_H
