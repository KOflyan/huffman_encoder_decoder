
#ifndef FINAL_DECODER_H
#define FINAL_DECODER_H

#include "../codebook/codebook.h"


using namespace std;

/**
 * Class for decoding text using canonical Huffman algorithm.
 */
class HuffmanDecoder {
    private:
        string fileContents;
        HuffmanCodeBook codeBook;
        /**
         * Reconstruct Huffman codebook from provided header.
         *
         * @param header Huffman header as string
         */
        void reconstructCodeBook(string header);
        /**
         * Given code, check if character with this code exists in a row.
         *
         * @param code binary code
         * @param row Huffman codebook row
         * @param fetchedBits
         * @return
         */
        char findCharMatchingCode(size_t code, HuffmanCodeBookRow &row, string &fetchedBits);

        pair<string, string> decodeFromHex(vector<string> headerAndContent);
    public:
        /**
         * Constructor.
         *
         * @param fileContents input file contents as vector of chars.
         */
        explicit HuffmanDecoder(string &fileContents);
        /**
         * Decode file contents and return as string.
         *
         * @return
         */
        string decodeAndReturn();
};

#endif //FINAL_DECODER_H
