
#include "decoder.h"
#include "../common/common.h"
#include "../common/utils.h"
#include <bitset>
#include <math.h>


HuffmanDecoder::HuffmanDecoder(string &fileContents) {
    this->fileContents = fileContents;
}


void HuffmanDecoder::reconstructCodeBook(string header) {

    vector<string> headerPieces = Utils::split(header, Constants::HEADER_SEPARATOR);
    vector<string> codes = Utils::split(Utils::erase(headerPieces.at(0), ','));
    vector<string> encodedContent = Utils::split(Utils::erase(headerPieces.at(1), ','));

    size_t index = 0;
    size_t code = 0;

    for (size_t i = 0; i < codes.size(); ++i) {
        string c = codes.at(i);
        size_t numCodes = stoul(c);

        string result = Utils::toBinaryString(code);
        code = (code + numCodes) << 1;

        vector<char> symbols;

        for (size_t j = index; j < index + numCodes; ++j) {
            symbols.push_back(encodedContent.at(j)[0]);
        }

        HuffmanCodeBookRow row(result, i, numCodes, symbols);
        this->codeBook.append(row);

        index += numCodes;
    }
}

char HuffmanDecoder::findCharMatchingCode(size_t code, HuffmanCodeBookRow &row, string &fetchedBits) {
    for (auto& c : row.symbols) {
        string binary = Utils::toBinaryString(code);
        if (binary == fetchedBits) {
            return c;
        }
        ++code;
    }

    return '\0';
}


string HuffmanDecoder::decodeAndReturn() {

    vector<string> headerAndContent = Utils::split(this->fileContents, Constants::CONTENT_SEPARATOR);

    if (headerAndContent.size() < 2) throw runtime_error("Invalid file format, cannot decode");

    string header, content; tie(header, content) = this->decodeFromHex(headerAndContent);

    this->reconstructCodeBook(header);
    string encoded = content, decoded, fetchedBits;

    size_t i = 0;
    while (i < encoded.size()) {
        fetchedBits += encoded.at(i);

        for (auto& row : this->codeBook.rows) {
            auto code = bitset<Constants::BITS_IN_LONG>(row.code).to_ulong();
            auto fetchedBitsValue = bitset<Constants::BITS_IN_LONG>(fetchedBits).to_ulong();

            if (fetchedBitsValue < code) {
                break;
            }

//            auto a = pow(2, floor(log2(code + row.symbols.size()) + 1));
//
//            if (fetchedBitsValue > a) continue;

            char decodedChar = this->findCharMatchingCode(code, row, fetchedBits);

            if (decodedChar != '\0') {
                decoded += decodedChar;
                fetchedBits = "";
                break;
            }
        }
        ++i;
    }

    return decoded;
}

pair<string, string> HuffmanDecoder::decodeFromHex(vector<string> headerAndContent) {
    string header, content;
    vector<string> headerHex = Utils::split(headerAndContent.at(0));
    vector<string> contentHex = Utils::split(headerAndContent.at(1));

    size_t ind = 0;
    for (int i = 0; i < 2; ++i) {

        vector<string> loopBy = i == 0 ? headerHex : contentHex;

        for (auto& val : loopBy) {
            if (i == 0) {
                long a = Utils::fromHex(val);
                header += (char) Utils::fromHex(val);
            } else {
                long numeric = Utils::fromHex(val);
                string bits = Utils::toBinaryString(static_cast<size_t>(numeric));
                content += ind == loopBy.size() - 1 ? bits : Utils::zfill(bits, 8);
                ++ind;
            }
        }
    }

    return make_pair(header, content);
}
