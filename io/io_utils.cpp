
#include "io_utils.h"
#include <fstream>


IOHolder::IOHolder() {
    this->is = &cin;
    this->os = &cout;
}


void IOHolder::setIO(istream *is, ostream *os) {
    this->is = is;
    this->os = os;
}


pair<string, string> IOProcessor::getInputAndOutput(po::variables_map &vm) {
    string in = "input", out = "output";

    if (!vm.count(in) && !vm.count(out)) return make_pair("", "");

    if (vm.count(in) && vm.count(out)) {
        return make_pair(vm[in].as<string>(), vm[out].as<vector<string>>().front());
    }

    if (vm.count(out) == 2) {
        return make_pair(vm[out].as<vector<string>>().back(), vm[out].as<vector<string>>().front());
    }

    throw runtime_error("Something went wrong while parsing args");
}


IOHolder IOProcessor::getIOStreams(po::variables_map &vm) {

    IOHolder result;
    pair<string, string> inOut = this->getInputAndOutput(vm);
    string in, out; tie(in, out) = inOut;
    if (!in.empty()) {
        auto ifs = new ifstream(in, ios::binary);
        auto ofs = new ofstream(out, ios::out);
        result.setIO(ifs, ofs);
    }

    return result;
}


string IOProcessor::readFile(istream &is) {
    string contents;
    char c;
    while (is.get(c)) {
        contents += c;
    }
    return contents;
}


void IOProcessor::writeFile(ostream *os, string &content) {
    *os << content;
    auto ofs = dynamic_cast<ofstream*>(os);
    if (ofs != nullptr) {
        ofs->close();
    }
}
