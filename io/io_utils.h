
#ifndef FINAL_IO_UTILS_H
#define FINAL_IO_UTILS_H

#include <iostream>
#include <vector>
#include <boost/program_options.hpp>

namespace po = boost::program_options;
using namespace std;

/**
 * Class for saving I/O streams.
 * By default streams are standard I/O streams, which we can reset if specific files are given.
 */
class IOHolder {
    public:
        istream *is;
        ostream *os;

        explicit IOHolder();
        /**
         * Set I/O streams.
         *
         * @param is input stream pointer
         * @param os output stream pointer
         */
        void setIO(istream *is, ostream *os);
};


/**
 * Class for processing cmd arguments, configuring streams, reading/writing.
 */
class IOProcessor {
    private:
        /**
         * Get input and output file names.
         *
         * @param vm boost variable map
         * @return pair<Input file name, Output file name>
         */
        pair<string, string> getInputAndOutput(po::variables_map &vm);
    public:
        explicit IOProcessor() = default;
        /**
         * Initialize I/O streams, save into IOHolder.
         *
         * @param vm boost variable map
         * @return IOHolder object
         */
        IOHolder getIOStreams(po::variables_map &vm);
        /**
         * Read input file, return contents as vector of chars.
         *
         * @param is input stream
         * @return vector of chars
         */
        string readFile(istream &is);

        /**
         * Write given string into file, using given output stream.
         *
         * @param os output stream
         * @param content string to write
         */
        void writeFile(ostream *os, string &content);
};

#endif //FINAL_IO_UTILS_H
