
#include <bitset>
#include "codebook.h"
#include "../common/common.h"
#include "../common/utils.h"


HuffmanCodeBookRow::HuffmanCodeBookRow(string &code, size_t index, size_t numberOfCodes, const vector<char> &symbols) {
    this->code = code;
    this->index = index;
    this->numberOfCodes = numberOfCodes;
    this->symbols = symbols;
}

string HuffmanCodeBookRow::getCharCode(size_t index) {
    size_t code = bitset<Constants::BITS_IN_LONG>(this->code).to_ullong();
    string stringBinary = bitset<Constants::BITS_IN_LONG>(code + index).to_string();
    return Utils::removeLeftSideZeros(stringBinary);
}

void HuffmanCodeBook::append(HuffmanCodeBookRow &row) {
    rows.push_back(row);

}

string HuffmanCodeBook::getCharCode(char c) {

    for (auto& row : this->rows) {
        auto match = find(row.symbols.begin(), row.symbols.end(), c);
        if (match != row.symbols.end()) {
            size_t index = static_cast<size_t>(match - row.symbols.begin());
            return row.getCharCode(index);
        }
    }

    throw runtime_error("No code found for char: " + to_string(c));
}
