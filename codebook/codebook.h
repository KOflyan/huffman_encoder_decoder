
#ifndef FINAL_CODEBOOK_H
#define FINAL_CODEBOOK_H

#include <iostream>
#include <vector>

using namespace std;

/**
 * Representation of single row in Huffman codebook.
 */
class HuffmanCodeBookRow {
    public:
        string code;
        size_t index;
        size_t numberOfCodes;
        vector<char> symbols;
        HuffmanCodeBookRow(string &code, size_t index, size_t numberOfCodes, const vector<char> &symbols);
        /**
         * Return binary code for char under provided index.
         *
         * @param index index of char in symbols
         * @return binary string
         */
        string getCharCode(size_t index);

};

/**
 * Representation of Huffman codebook.
 */
class HuffmanCodeBook {
    public:
        vector<HuffmanCodeBookRow> rows;
        HuffmanCodeBook() = default;
        /**
         * Append row to Huffman codebook.
         *
         * @param row HuffmanCodebookRow instance.
         */
        void append(HuffmanCodeBookRow &row);
        /**
         * Get binary code for provided char.
         *
         * @param c character
         * @return binary string
         */
        string getCharCode(char c);
};

#endif //FINAL_CODEBOOK_H
