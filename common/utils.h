
#ifndef FINAL_UTILS_H
#define FINAL_UTILS_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <bitset>

using namespace std;

/**
 * Utility functions defined here.
 */
class Utils {
    private:
        explicit Utils() = default;
    public:
        /**
         * Convert sequence of bits to hexadecimal string.
         *
         * @param bits sequence of bits
         * @return hex value
         */
        static string toHex(string &bits);
        /**
         * Convert decimal number to hexadecimal string.
         *
         * @param num number
         * @return hex value
         */
        static string toHex(int num);
        /**
         * Convert number back from hexadecimal to decimal.
         *
         * @param hex hexadecimal string.
         * @return
         */
        static long fromHex(string &hex);
        /**
         * Remove spaces from string (left side).
         *
         * @param s string
         */
        static void ltrim(string &s);
        /**
         * Remove spaces from string (right side).
         *
         * @param s
         */
        static void rtrim(string &s);
        /**
         * Fill string with zeroes from the left side, until it is of specified length.
         *
         * @param s string
         * @param length desired length
         * @return changed string with zeroes
         */
        static string zfill(string &s, long length);
        /**
         * Removes left side zeros from string.
         * Example: 0000000000000010100 -> 10100
         *
         * @param s binary string
         * @return binary string without excessive zeros
         */
        static string removeLeftSideZeros(string &s);
        /**
         * Erase all occurrences of char from string.
         *
         * @param s input string
         * @param c char
         * @return modified string
         */
        static string erase(string &s, char c);
        /**
         * Split string by provided delimiter.
         *
         * @param s input string
         * @param delimiter string to split by
         * @return vector of strings
         */
        static vector<string> split(const string &s, const string &delimiter=" ");
        /**
         * Transform number into binary string.
         *
         * @param num number
         * @return binary string
         */
        static string toBinaryString(size_t num);
        /**
         * Concatenate elements of vector into single string, separated by provided delimiter.
         *
         * @tparam T whatever type vector is
         * @param v input vector
         * @param delimiter elements will be separated by this string
         * @return string of all elements
         */
        template <typename T>
        static string join(vector<T> &v, const string &delimiter="") {
            string result;
            for (size_t i = 0; i < v.size(); i++) {
                result += v.at(i);
                if (i < v.size() - 1) {
                    result += delimiter;
                }
            }
            return result;
        }
};

#endif //FINAL_UTILS_H
