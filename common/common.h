
#ifndef FINAL_COMMON_H
#define FINAL_COMMON_H

#include <iostream>
#include <map>
#include <queue>


using namespace std;

/**
 * Various global constants defined here.
 */
class Constants {
    public:
        /** Bits in long  */
        static int const BITS_IN_LONG = sizeof(long) * 8;
        /** Header separator (delimits first Huffman header part from second) */
        static string const HEADER_SEPARATOR;
        /** Content separator (delimits Huffman header from file contents) */
        static string const CONTENT_SEPARATOR;
};

template <typename T>
ostream &operator<<(ostream &os, vector<T> &v) {
    for (auto& e : v) {
        os << e << " ";
    }
    return os;
}


template <typename K, typename V>
ostream& operator<<(ostream& os, map<K, V> const& m) {
    for (const auto &p : m) {
        os << "{" << p.first << ": " << p.second << "}\n";
    }
    return os;
}


#endif //FINAL_COMMON_H
