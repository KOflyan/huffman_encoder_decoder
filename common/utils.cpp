
#include <sstream>
#include "utils.h"
#include "common.h"


string Utils::removeLeftSideZeros(string &s) {
    string result = s.erase(0, min(s.find_first_not_of('0'), s.size() - 1));
    return result.size() > 1 ? result : "0" + result;
}

string Utils::erase(string &s, char c) {
    s.erase(remove(s.begin(), s.end(), c), s.end());
    return s;
}

vector<string> Utils::split(const string &s, const string &delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    string token;
    vector<string> res;

    while ( (pos_end = s.find(delimiter, pos_start)) != string::npos) {
        token = s.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back(token);
    }

    res.push_back(s.substr(pos_start));
    return res;
}

string Utils::toBinaryString(size_t num) {
    string binary = bitset<Constants::BITS_IN_LONG>(num).to_string();
    return Utils::removeLeftSideZeros(binary);
}


string Utils::toHex(string &bits) {
    bitset<8> b(bits);
    stringstream buffer;
    buffer << hex << b.to_ulong();
    return buffer.str();
}

string Utils::toHex(int num) {
    stringstream buffer;
    buffer << hex << num;
    return buffer.str();
}

long Utils::fromHex(string &hex) {
    return stoul(hex, nullptr, 16);
}


void Utils::ltrim(string &s) {
    s.erase(s.begin(), find_if(s.begin(), s.end(),
                                    not1(ptr_fun<int, int>(isspace))));
}

void Utils::rtrim(string &s) {
    s.erase(find_if(s.rbegin(), s.rend(),
                         not1(ptr_fun<int, int>(isspace))).base(), s.end());
}

string Utils::zfill(string &s, long length) {
    s = string(length - s.length(), '0') + s;
    return s;
}
