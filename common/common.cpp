
#include "common.h"


/** Separates header from file contents. */
const string Constants::CONTENT_SEPARATOR = "|";
/** Separates parts of the header. */
const string Constants::HEADER_SEPARATOR = " ::: ";