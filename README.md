`WIP`
===


INSTALLATION
============

You have to download and build `Boost` libraries, and specify path to them in `CMakeLists.txt`:

`set(BOOST_ROOT "<your path>")`

After that you're good to go.

USAGE
=====

List available options:
```bash
./huffman_encoder_decoder --help
```

Encode:
```bash
./huffman_encoder_decoder -i input -o output
```

Decode:
```bash
./huffman_encoder_decoder -i encoded -o -decoded --decode
```

Alternatives:

```bash
./huffman_encoder_decoder -o input output
```

```bash
./huffman_encoder_decoder < input > output
```


Example:

```bash

./huffman_encoder_decoder -i example/input_normal -o example/input_encoded

./huffman_encoder_decoder -i example/input_encoded -o example/input_decoded --decode

```


DOCUMENTATION
==============

To generate documentation, run:

```bash
doxygen && cd docs/latex && make
```

Find html documentation in `docs/html` and pdf file in
`docs/latex/refman.pdf`